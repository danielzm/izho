var database = [];

function getArtistMatch() {
  var artistsMatch = {};
  _.each(database, function (item) {
    artistsMatch[item.artista] = artistsMatch[item.artista] || [];
    artistsMatch[item.artista].push(item.username);
  });
  return artistsMatch;
}

function getAllUsers(database) {
  var users = {};
  _.each(database, function (item) {
    users[item.username] = true;
  });
  return _.keys(users);
}

function getAllMatchesCount(database) {

}

// requiere username
// retorna matches
/*
    [{
      user: 'sandra',
      artist: 'vicente',
    }, {
      user: 'sandra',
      artist: 'bruno mars',
    }]
*/

function getUsernameMatches(username) {
  var artistMatch = getArtistMatch(),
    matches = [];

  _.each(artistMatch, function (users, artist) {
    var userIndex = _.indexOf(users, username);

    // si no esta el usuario sales
    if (userIndex < 0) {
      return;
    }

    // si quitando al usuario no hay mas usuarios sale
    var indMatch = _.without(users, username);
    if (indMatch.length < 0) {
      return;
    }

    _.each(indMatch, function (username) {
      matches.push({
        'username': username,
        'artist': artist
      });
    });
  });

  return {
    username: username,
    matches: matches
  };
};

function fillTotalUsersBadge(allUsers) {
  $('#total-users span').html(allUsers.length);
}

function fillTotalMatchesBadge(allMatches) {
  $('#total-matches span').html(allMatches);
}

function buildCountBadge(count) {
  $('#match-count').html(count);
}

function buildMatchTable(matchdata) {

  if (!matchdata) {
    return;
  }

  $('#tablematches').remove();
  var $table = $('<table id="tablematches" class="table table-striped hidden"></table>'),
    $th = $('<thead>' +
      '<tr>' +
      '<td>Persona</td>' +
      '<td>Artista</td>' +
      '</tr>' +
      +'</thead>');
  _.each(matchdata.matches, function (userObj) {
    var $row = $('<tr></tr>'),
      $usernameCol = $('<td></td>'),
      $artistCol = $('<td></td>');
    $table.append($th);

    $usernameCol.html(userObj.username);
    $artistCol.html(userObj.artist);

    $row.append($usernameCol);
    $row.append($artistCol);
    $table.append($row);
  });

  $('#matches .row.matches .wrapper').append($table);
}


$(function () {

  $.getJSON('https://izho.firebaseio.com/items.json', function matrix(data) {
    data.shift();
    database = data;
    var allUsers = getAllUsers(database),
      allMatches = getAllMatchesCount(database);

    fillTotalUsersBadge(allUsers);
    fillTotalMatchesBadge(allMatches);
  });

  $('#match-total').on('click', function () {
    $('#tablematches').toggleClass('hidden');
  });

  $('#search').on('click', function (event) {
    event.preventDefault();
    var username = $('input#username').val(),
      matchResults = getUsernameMatches(username);

    buildMatchTable(matchResults);
    buildCountBadge(matchResults.matches.length);
  })
});
